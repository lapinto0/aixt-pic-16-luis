// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: machine.c
// Author: Fernando Martínez Santa
// Date: 2023
// License: MIT
//
// Description: This is a module include all the emulated machine modules. 
#include "./machine/pin.c"
#include "./machine/pwm.c"
#include "./machine/adc.c"
